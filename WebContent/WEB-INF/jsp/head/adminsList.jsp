<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>导航</title>
	</head>
	<body>
		<table width="90%" border="1">
			<tr>
				<td>序号</td>
				<td>Id</td>
				<td>email</td>
				<td>phone</td>
				<td>qq</td>
				<td>状态</td>
				<td>创建时间</td>
				<td>更新时间</td>
				<td>上次登陆时间</td>
			</tr>
			<c:forEach items="${requestScope.adminsList }" var="admins" varStatus="stat">
				<tr>
					<td>${stat.count }</td>
					<td>${admins.id }</td>
					<td>${admins.email }</td>
					<td>${admins.phone }</td>
					<td>${admins.qq }</td>
					<td>${admins.statusStr }</td>
					<td><fmt:formatDate value="${admins.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${admins.updateTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${admins.lastLoginTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>