<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${requestScope.items.name }-Sxt</title>
	<%@ include file="/common/include/title.jsp" %>
</head>
<body>
	<!-- 动态导入 -->
	<jsp:include page="/header.mvc" />
	<div class="infos-wrap">
	<div class="fish-left main-left">
		<div class="mbox tshow_index">
			<div class="title bor_bot fixed">
				<p class="items fl" title="心理测试"></p>
				<div class="tinfo fl">
					<h2>${requestScope.items.name }</h2>
					<span class="icons" title="测试人数"></span>${requestScope.items.testedNum }人测试过 &nbsp; 
					<span class="icons fav" title="收藏次数"></span>${requestScope.items.favNum }次收藏 &nbsp; 
					<span class="icons tdates" title="发布时间"></span><fmt:formatDate value="${requestScope.items.pubTime }" 
					pattern="yyyy-MM-dd HH:mm:ss"/> &nbsp; 
					<span class="icons comments" title="评论数"></span> 
					<a href="#comments">${requestScope.items.suggNum }条评论</a> 
					<span class="total">本测试共 
						<span class="fgreen fb">${requestScope.items.questionNum }</span> &nbsp;题
					</span>
				</div>
			</div>
			<div class="tshow bor_top">
				<p class="pbox">
					<img src="${requestScope.items.imgPath }" title="${requestScope.items.name }"
						alt="${requestScope.items.name }" width="400">
				</p>
				<p class="tdesc">${requestScope.items.content }</p>


				<p class="tbtns">
					<a href="${rootPath }/items/${requestScope.items.id}/start.mvc" class="test_btn"
						target="_self">开始测试</a>
				</p>

				<p class="test_tips">
					<span class="fgrey">此测试仅供娱乐，不做专业指导！</span>
				</p>
			</div>
		</div>
		<!--测试显示首页-->
		<div class="shares nshares">
			<span class="icons fav"></span> <a
				href="http://www.xinli001.com/ceshi/280/favorite" class="add_fav"
				onclick="return favorite(this)">喜欢？！收藏</a>
			<div class="bdsharebuttonbox sbar bdshare-button-style0-24"
				data-bd-bind="1489633354620">
				<a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
				<a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
				<a href="#" class="bds_sqq" data-cmd="sqq" title="分享到QQ好友"></a> <a
					href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a> <a
					href="#" class="bds_douban" data-cmd="douban" title="分享到豆瓣网"></a> <a
					class="bds_count" data-cmd="count" title="累计分享6次">6</a> <a href="#"
					class="bds_more" data-cmd="more"></a>
			</div>
			<script>
				window._bd_share_config = {
					"common" : {
						"bdSnsKey" : {},
						"bdText" : "",
						"bdMini" : "1",
						"bdMiniList" : false,
						"bdPic" : "",
						"bdStyle" : "0",
						"bdSize" : "24"
					},
					"share" : {}
				};
				with (document)
					0[(getElementsByTagName('head')[0] || body)
							.appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89343201.js?cdnversion='
							+ ~(-new Date() / 36e5)];
			</script>
		</div>

		<div class="prevnext">
			<span class="fgrey">上一篇：</span>
			<c:forEach items="${requestScope.itemsPreList }" var="items" varStatus="stat" begin="0" end="0" step="1">
				<a href="${rootPath }/items/${items.id}.mvc" class="prev" title="爱与喜欢甄别器">
					${items.name }
				</a> 
			</c:forEach>
			<span class="r">
				<span class="fgrey">下一篇：</span>
				<c:forEach items="${requestScope.itemsNextList }" var="items" varStatus="stat" begin="0" end="0" step="1">
					<a href="${rootPath }/items/${items.id}.mvc" class="prev" title="爱与喜欢甄别器">
						${items.name }
					</a> 
				</c:forEach>
			</span>
		</div>

		<div class="mbox comments_show" id="comments_area">
			<h2>
				发表留言 ：<a name="comments"></a>
			</h2>
			<form id="id_comment_form" class="comments_form" style=""
				action="http://www.xinli001.com/ceshi/280/comment" method="post"
				onsubmit="return false">
				<textarea id="id_comment_content" class="txt" name="content"
					nullmsg="请输入留言内容"></textarea>
				<p class="cbtns">
					<span id="id_comment_content_msg" class="inputmsg"></span> <input
						id="id_comment_submit" class="btns" value="发表留言" type="submit">
				</p>
			</form>
		</div>
		<div id="id_comment_page">
			<div class="mbox comments_list fixed" id="comments_list">
				<div class="items" id="id_comment_3149788">
					<div class="ct">
						<span class="arrow"></span> <a
							href="http://www.xinli001.com/user/1004682070"
							class="nickname ua">xinli_4011</a> ：[2017-03-16 09:49:16]
					</div>
					<div class="c_contents fixed">
						<div class="pbox fl">
							<a href="http://www.xinli001.com/user/1004682070"> <img
								src="http://ossimg.xinli001.com/20160329/9fe4842afb2c58c2200a1b9ae7a68ca9.png!50"
								alt="xinli_4011" hover="true" width="50" height="50">
							</a>
						</div>
						<div class="cshow">20</div>
					</div>
				</div>
				
				<!--items-->
			</div>
			<!--end-->
			<!--留言列表-->
			<div class="pagebar" id="pages">
				<span>1</span> |<a href="#" onclick="return getItemsByPage(2)"
					target="_self">2</a> |<a href="#"
					onclick="return getItemsByPage(3)" target="_self">3</a> |<a
					href="#" onclick="return getItemsByPage(4)" target="_self">4</a> |<a
					href="#" onclick="return getItemsByPage(5)" target="_self">5</a> |<a
					href="#" onclick="return getItemsByPage(6)" target="_self">6</a> |<a
					href="#" onclick="return getItemsByPage(2)" class="next"
					target="_self">下一页</a>
			</div>

		</div>
	</div>
	<!--left-->

	<div class="fish-left main-right">
		<div class="multi_ads fixed">
			<div class="mt">
				<strong>编辑推荐<span class="mq">：</span><span class="ms">······
				</span></strong>
			</div>
			<dl>
				<dd>
					<a href="http://www.xinli001.com/info/100360756?from=cheshi"
						title="心探社" target="_blank"> <img
						src="http://ossimg.xinli001.com/20161219/22c35b0aef597efe978b7a084dcf3030.jpg"
						hover="true" title="心探社" alt="心探社" width="135" height="135">
					</a>
				</dd>

				<dd>
					<a href="http://www.xinli001.com/ceshi/99897526?from=2017"
						title="心理需求测评" target="_blank"> <img
						src="http://image.xinli001.com/20160805/030954q4tnzb0jb0nyq06s.jpg"
						hover="true" title="心理需求测评" alt="心理需求测评" width="135" height="135">
					</a>
				</dd>

				<dt>
					<a href="http://www.xinli001.com/ceshi/99897572?from=ceshi"
						title="抑郁症测试" target="_blank"> <img
						src="http://ossimg.xinli001.com/20161024/16f19e49dab19387649e80eae35df090.jpg"
						hover="true" title="抑郁症测试" alt="抑郁症测试" width="280" height="135">
					</a>
				</dt>
				<br class="clear">
			</dl>
		</div>
		<!-- 2-->
		<div class="test_rmd_list fixed">
			<div class="st rbor">
				<span class="fb">性格测试推荐 ：</span><span class="more"><a
					href="/ceshi">更多»</a></span>
			</div>
			<dl>
				<dd>
					<p class="pbox fl">
						<a href="http://www.xinli001.com/ceshi/280"> <img
							src="http://ossimg.xinli001.com/20170315/235153426662dd610611c691d794529f.jpg!90x60"
							hover="true" alt="测试你的虚伪指数" width="90" height="60">
						</a>
					</p>
					<p class="tinfo fl">
						<a href="http://www.xinli001.com/ceshi/280">测试你的虚伪指数</a> <span
							class="total"><span class="icons" title="测试人数"></span>66170人测试过</span>
					</p>
				</dd>
				<dd>
					<p class="pbox fl">
						<a href="http://www.xinli001.com/ceshi/265"> <img
							src="http://ossimg.xinli001.com/20170314/bf18ebf47f0e122ee65f1f6070489168.jpg!90x60"
							hover="true" alt="你讨厌什么样的自己？" width="90" height="60">
						</a>
					</p>
					<p class="tinfo fl">
						<a href="http://www.xinli001.com/ceshi/265">你讨厌什么样的自己？</a> <span
							class="total"><span class="icons" title="测试人数"></span>33330人测试过</span>
					</p>
				</dd>
				<dd>
					<p class="pbox fl">
						<a href="http://www.xinli001.com/ceshi/882"> <img
							src="http://image.xinli001.com/20160704/074731hd6ysi5rfvguuy8a.jpg!90x60"
							hover="true" alt="测测最适合你的城市类型是什么样子的？" width="90" height="60">
						</a>
					</p>
					<p class="tinfo fl">
						<a href="http://www.xinli001.com/ceshi/882">测测最适合你的城市类型是什么样子的？</a>
						<span class="total"><span class="icons" title="测试人数"></span>114116人测试过</span>
					</p>
				</dd>
				<dd>
					<p class="pbox fl">
						<a href="http://www.xinli001.com/ceshi/214"> <img
							src="http://ossimg.xinli001.com/20170310/8f6a282323cccca31b678ff8209844c7.jpg!90x60"
							hover="true" alt="测试你能看见哪种幻想生物" width="90" height="60">
						</a>
					</p>
					<p class="tinfo fl">
						<a href="http://www.xinli001.com/ceshi/214">测试你能看见哪种幻想生物</a> <span
							class="total"><span class="icons" title="测试人数"></span>23014人测试过</span>
					</p>
				</dd>
				<dd>
					<p class="pbox fl">
						<a href="http://www.xinli001.com/ceshi/99897673"> <img
							src="http://ossimg.xinli001.com/20170306/146a1564eef13aa62adec2952e196043.jpg!90x60"
							hover="true" alt="测测你像哪个影视剧中的经典角色？我是..." width="90" height="60">
						</a>
					</p>
					<p class="tinfo fl">
						<a href="http://www.xinli001.com/ceshi/99897673">测测你像哪个影视剧中的经典角色？我是...</a>
						<span class="total"><span class="icons" title="测试人数"></span>13463人测试过</span>
					</p>
				</dd>
			</dl>
		</div>
		<div class="tags fixed">
			<div class="st rbor">
				<span class="fb">热门标签 ：</span>
			</div>
			<ul>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E6%80%A7%E6%A0%BC%E6%B5%8B%E8%AF%95">性格测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E7%88%B1%E6%83%85%E6%B5%8B%E8%AF%95">爱情测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E8%83%BD%E5%8A%9B%E6%B5%8B%E8%AF%95">能力测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E5%BF%83%E7%90%86%E6%B5%8B%E8%AF%95">心理测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E8%B6%A3%E5%91%B3%E6%B5%8B%E8%AF%95">趣味测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E4%B8%93%E4%B8%9A%E6%B5%8B%E8%AF%95">专业测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E5%A7%BB%E7%BC%98%E6%B5%8B%E8%AF%95">姻缘测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E6%B5%8B%E8%AF%95%E7%88%B1%E6%83%85">测试爱情</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E8%81%8C%E4%B8%9A%E6%B5%8B%E8%AF%95">职业测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E9%A2%84%E8%A8%80%E6%B5%8B%E8%AF%95">预言测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E4%BC%9A%E5%91%98%E6%B5%8B%E8%AF%95">会员测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E7%88%B1%E6%83%85">爱情</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E7%BF%BB%E8%AF%91%E6%B5%8B%E8%AF%95">翻译测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E4%B8%93%E4%B8%9A%E9%87%8F%E8%A1%A8">专业量表</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E6%BD%9C%E6%84%8F%E8%AF%86%E6%B5%8B%E8%AF%95">潜意识测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E5%A4%96%E5%9B%BD%E6%B5%8B%E8%AF%95">外国测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E6%80%A7%E6%A0%BC">性格</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E8%81%8C%E5%9C%BA%E6%B5%8B%E8%AF%95">职场测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E5%81%A5%E5%BA%B7%E6%B5%8B%E8%AF%95">健康测试</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E5%BF%83%E7%90%86%E5%81%A5%E5%BA%B7">心理健康</a></li>
				<li class="fl"><a
					href="http://www.xinli001.com/ceshi/tag?name=%E6%81%8B%E7%88%B1">恋爱</a></li>
			</ul>
		</div>
		<!--标签-->
	</div>
	<!--right-->
</div>
<!-- 静态导入 -->
<%@ include file="/common/include/footer.jsp" %>
</body>
</html>