<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
	
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="">
<meta name="format-detection" content="telephone=no">
<title>${requestScope.columns.name }_题_答案_大全-壹心理Sxt</title>
<meta name="keywords" content="能力测试,能力测试题及答案,能力测试题,能力测试大全">
<meta name="description" content="壹心理能力测试，提供专业的能力测试题及答案，帮助你了解自己。">

<link rel="stylesheet" href="css/index.min.css">
<script src="common/resource/jquery-3.1.1.min.js"></script>



</head>
<body>
	<!-- 动态导入 -->
	<jsp:include page="/header.mvc" />
	
	<div class="infos-wrap">
		<div class="fish-left main-left">
			<div class="list_rmd">
				<div class="title bor_bot">能力测试人气推荐</div>
				<div class="lists bor_top fixed">
					<ul>
						<li class="fl payTest"><a
							href="http://www.xinli001.com/ceshi/177"> <img
								src="http://ossimg.xinli001.com/20170215/7e404600768e73d3caf27ff85ec15a92.jpg!180x120"
								width="180" height="120" hover="true" alt="测测你的气场有多强？">
								<p>测测你的气场有多强？</p>
						</a></li>
						<li class="fl payTest"><a
							href="http://www.xinli001.com/ceshi/1581"> <img
								src="http://ossimg.xinli001.com/20170125/a8ee488a96f14231a1542b8fffe9f0bd.jpg!180x120"
								width="180" height="120" hover="true" alt="测你春节能收到多大红包？">
								<p>测你春节能收到多大红包？</p>
						</a></li>
						<li class="fl payTest"><a
							href="http://www.xinli001.com/ceshi/147"> <img
								src="http://ossimg.xinli001.com/20170117/1599723e9370a62389f104905bd07929.jpg!180x120"
								width="180" height="120" hover="true" alt="你的梦想离现实有多远？">
								<p>你的梦想离现实有多远？</p>
						</a></li>
					</ul>
				</div>
			</div>
			<!--推荐-->
			<h2 class="list_title">${requestScope.columns.name }：</h2>
			<div class="list_show">
				<dl>
					<c:forEach items="${requestScope.itemsList }" var="items" varStatus="stat">
					<dd class="fixed">
						<p class="pbox fl">
							<a href="${rootPath }/items/${items.id}.mvc"> <img class="lazyload"
							src="${items.imgPath }" width="120" height="80" alt="${items.name }" title="${items.name }" hover="true" />
							</a>
						</p>
						<p class="linfo fl">
							<a href="${rootPath }/items/${items.id}.mvc">${items.name }</a><br />
							${fn:substring(items.content,0,80) }...<br />
							<span class="total"><span class="icons" title="测试人数"></span>${items.testedNum }人测试过</span>
						</p>
					</dd>
					</c:forEach>
					
				</dl>
			</div>
			<!--列表-->
			<div class="pagebar" id="pages">
				<c:choose>
					<c:when test="${requestScope.pageInfoUtil.totalPage > 0 && requestScope.pageInfoUtil.totalPage < 5}">
						<c:forEach begin="1" end="${requestScope.pageInfoUtil.totalPage }" step="1" var="page">
							<c:choose>
								<c:when test="${requestScope.pageInfoUtil.currentPage == page }">
									<span>${page }</span> 
								</c:when>
								<c:otherwise>
									|<a href="${rootPath }/${requestScope.columns.url}.mvc?page=${page}" target="_self">${page }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:forEach begin="1" end="5" step="1" var="page">
							<c:choose>
								<c:when test="${requestScope.pageInfoUtil.currentPage == page }">
									<span>${page }</span> 
								</c:when>
								<c:otherwise>
									|<a href="${rootPath }/${requestScope.columns.url}.mvc?page=${page }" target="_self">${page }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						|<span>...</span>
						<c:if test="${requestScope.pageInfoUtil.currentPage > 5 && requestScope.pageInfoUtil.currentPage < requestScope.pageInfoUtil.totalPage - 3 }">
							|<span>${requestScope.pageInfoUtil.currentPage }</span> 
							|<span>...</span> 
						</c:if>
						<c:forEach begin="${requestScope.pageInfoUtil.totalPage - 3}" end="${requestScope.pageInfoUtil.totalPage}" step="1" var="page">
							<c:choose>
								<c:when test="${requestScope.pageInfoUtil.currentPage == page }">
									<span>${page }</span> 
								</c:when>
								<c:otherwise>
									|<a href="${rootPath }/${requestScope.columns.url}.mvc?page=${page}" target="_self">${page }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				|<a href="${rootPath }/${requestScope.columns.url}.mvc?page=${requestScope.pageInfoUtil.nextPage }" class="next" target="_self">下一页</a>
			</div>
		</div>
		<!--left-->
		<div class="fish-left main-right">
			<div class="multi_ads fixed">
				<div class="mt">
					<strong>编辑推荐<span class="mq">：</span><span class="ms">······
					</span></strong>
				</div>
				<dl>
					<dd>
						<a href="http://www.xinli001.com/info/100360756?from=cheshi"
							title="心探社" target="_blank"> <img
							src="http://ossimg.xinli001.com/20161219/22c35b0aef597efe978b7a084dcf3030.jpg"
							width="135" height="135" hover="true" title="心探社" alt="心探社">
						</a>
					</dd>

					<dd>
						<a href="http://www.xinli001.com/ceshi/99897526?from=2017"
							title="心理需求测评" target="_blank"> <img
							src="http://image.xinli001.com/20160805/030954q4tnzb0jb0nyq06s.jpg"
							width="135" height="135" hover="true" title="心理需求测评" alt="心理需求测评">
						</a>
					</dd>

					<dt>
						<a href="http://www.xinli001.com/ceshi/99897572?from=ceshi"
							title="抑郁症测试" target="_blank"> <img
							src="http://ossimg.xinli001.com/20161024/16f19e49dab19387649e80eae35df090.jpg"
							width="280" height="135" hover="true" title="抑郁症测试" alt="抑郁症测试">
						</a>
					</dt>
					<br class="clear">
				</dl>
			</div>
			<!-- 3-->
			<div class="test_rmd_list fixed">
				<div class="st rbor">
					<span class="fb">能力测试推荐 ：</span><span class="more"><a
						href="/ceshi">更多&raquo;</a></span>
				</div>
				<dl>
					<dd>
						<p class="pbox fl">
							<a href="http://www.xinli001.com/ceshi/177"> <img
								src="http://ossimg.xinli001.com/20170215/7e404600768e73d3caf27ff85ec15a92.jpg!90x60"
								width="90" height="60" hover="true" alt="测测你的气场有多强？" />
							</a>
						</p>
						<p class="tinfo fl">
							<a href="http://www.xinli001.com/ceshi/177">测测你的气场有多强？</a> <span
								class="total"><span class="icons" title="测试人数"></span>213509人测试过</span>
						</p>
					</dd>
					<dd>
						<p class="pbox fl">
							<a href="http://www.xinli001.com/ceshi/1581"> <img
								src="http://ossimg.xinli001.com/20170125/a8ee488a96f14231a1542b8fffe9f0bd.jpg!90x60"
								width="90" height="60" hover="true" alt="测你春节能收到多大红包？" />
							</a>
						</p>
						<p class="tinfo fl">
							<a href="http://www.xinli001.com/ceshi/1581">测你春节能收到多大红包？</a> <span
								class="total"><span class="icons" title="测试人数"></span>188772人测试过</span>
						</p>
					</dd>
					<dd>
						<p class="pbox fl">
							<a href="http://www.xinli001.com/ceshi/147"> <img
								src="http://ossimg.xinli001.com/20170117/1599723e9370a62389f104905bd07929.jpg!90x60"
								width="90" height="60" hover="true" alt="你的梦想离现实有多远？" />
							</a>
						</p>
						<p class="tinfo fl">
							<a href="http://www.xinli001.com/ceshi/147">你的梦想离现实有多远？</a> <span
								class="total"><span class="icons" title="测试人数"></span>68030人测试过</span>
						</p>
					</dd>
					<dd>
						<p class="pbox fl">
							<a href="http://www.xinli001.com/ceshi/99897654"> <img
								src="http://ossimg.xinli001.com/20170109/594435421d04dbbecb523d022a8645b7.jpg!90x60"
								width="90" height="60" hover="true" alt="记忆测试丨哪张图片才是对的？" />
							</a>
						</p>
						<p class="tinfo fl">
							<a href="http://www.xinli001.com/ceshi/99897654">记忆测试丨哪张图片才是对的？</a>
							<span class="total"><span class="icons" title="测试人数"></span>48431人测试过</span>
						</p>
					</dd>
					<dd>
						<p class="pbox fl">
							<a href="http://www.xinli001.com/ceshi/99897653"> <img
								src="http://ossimg.xinli001.com/20170109/a35a26102741701042a855a1ad00fd14.jpg!90x60"
								width="90" height="60" hover="true" alt="测你能否鸡年行大运？" />
							</a>
						</p>
						<p class="tinfo fl">
							<a href="http://www.xinli001.com/ceshi/99897653">测你能否鸡年行大运？</a> <span
								class="total"><span class="icons" title="测试人数"></span>32157人测试过</span>
						</p>
					</dd>
				</dl>
			</div>
			<div class="tags fixed">
				<div class="st rbor">
					<span class="fb">热门标签 ：</span>
				</div>
				<ul>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%80%A7%E6%A0%BC%E6%B5%8B%E8%AF%95">性格测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E7%88%B1%E6%83%85%E6%B5%8B%E8%AF%95">爱情测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%83%BD%E5%8A%9B%E6%B5%8B%E8%AF%95">能力测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%BF%83%E7%90%86%E6%B5%8B%E8%AF%95">心理测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%B6%A3%E5%91%B3%E6%B5%8B%E8%AF%95">趣味测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E4%B8%93%E4%B8%9A%E6%B5%8B%E8%AF%95">专业测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%A7%BB%E7%BC%98%E6%B5%8B%E8%AF%95">姻缘测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%B5%8B%E8%AF%95%E7%88%B1%E6%83%85">测试爱情</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%81%8C%E4%B8%9A%E6%B5%8B%E8%AF%95">职业测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E9%A2%84%E8%A8%80%E6%B5%8B%E8%AF%95">预言测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E4%BC%9A%E5%91%98%E6%B5%8B%E8%AF%95">会员测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E7%88%B1%E6%83%85">爱情</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E7%BF%BB%E8%AF%91%E6%B5%8B%E8%AF%95">翻译测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E4%B8%93%E4%B8%9A%E9%87%8F%E8%A1%A8">专业量表</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%BD%9C%E6%84%8F%E8%AF%86%E6%B5%8B%E8%AF%95">潜意识测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%A4%96%E5%9B%BD%E6%B5%8B%E8%AF%95">外国测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%80%A7%E6%A0%BC">性格</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%81%8C%E5%9C%BA%E6%B5%8B%E8%AF%95">职场测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%81%A5%E5%BA%B7%E6%B5%8B%E8%AF%95">健康测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%BF%83%E7%90%86%E5%81%A5%E5%BA%B7">心理健康</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%81%8B%E7%88%B1">恋爱</a></li>
				</ul>
			</div>
			<!--标签-->
		</div>
		<!--right-->
	</div>
<%@ include file="/common/include/footer.jsp" %>
</html>