<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE html>
<html>
<head>

<title>Sxt-壹心理</title>
<meta name="keywords" content="心理测试,心理测试题及答案,心理测试题,心理测试大全">
<meta name="description"
	content="壹心理心理测试频道，这里有爱情测试、性格测试、能力测试、会员测试、精选测试、专业测评，包含了心理测试题及答案。">
	
<%@ include file="/common/include/title.jsp" %>

</head>
<body>
	<%-- <%@ include file="/common/include/header.jsp" %> --%>
	<jsp:include page="/header.mvc" />
	<div class="infos-wrap">
		<div class="fish-left main-left">
			<div class="focus">
				<h2>发现未知的自己 ：</h2>
				<div class="focus_box">
					<div class="focus_show" id="rolls_imgs">
						<div id="rolls">
							<div class="roll_box">
								<dl>
									<dt>
										<p class="pbox">
											<a href="http://www.xinli001.com/ceshi/99897673"><img
												src="http://ossimg.xinli001.com/20170306/146a1564eef13aa62adec2952e196043.jpg!200x134"
												width="200" height="134" hover="true"
												alt="测测你像哪个影视剧中的经典角色？我是..."></a>
										</p>
										<p class="pinfo">
											<a href="http://www.xinli001.com/ceshi/99897673" class="t">测测你像哪个影视剧中的经典角色？我是...</a><br>
											<span class="fgrey">测试人数：</span>12657人<br> <span
												class="fgrey">评论：</span>45条<br> <a
												href="http://www.xinli001.com/ceshi/99897673/start"
												class="start">开始测试</a>
										</p>

									</dt>
									<dd>
										影视女主千千万，测测你是哪一款！作为一名新时代女性，你会拥有哪种人格魅力？你不可触碰的底线又是什么？... <a
											href="http://www.xinli001.com/ceshi/99897673">[详细]</a>
									</dd>
								</dl>
							</div>
							<div class="roll_box">
								<dl>
									<dt>
										<p class="pbox">
											<a href="http://www.xinli001.com/ceshi/186"><img
												src="http://ossimg.xinli001.com/20170303/eefe9ff5c994502af90b5bbf6127ea50.jpg!200x134"
												width="200" height="134" hover="true" alt="什么样的异性和你最登对？"></a>
										</p>
										<p class="pinfo">
											<a href="http://www.xinli001.com/ceshi/186" class="t">什么样的异性和你最登对？</a><br>
											<span class="fgrey">测试人数：</span>257432人<br> <span
												class="fgrey">评论：</span>707条<br> <a
												href="http://www.xinli001.com/ceshi/186/start" class="start">开始测试</a>
										</p>

									</dt>
									<dd>
										每个人都有自己独特的个性，恋人间最大的麻烦也许就是“相恋容易相处难”，想知道怎样的异性和你最配、最“... <a
											href="http://www.xinli001.com/ceshi/186">[详细]</a>
									</dd>
								</dl>
							</div>
							<div class="roll_box">
								<dl>
									<dt>
										<p class="pbox">
											<a href="http://www.xinli001.com/ceshi/99897663"><img
												src="http://ossimg.xinli001.com/20170219/dd9d7bd2913207788d041e8aa736ab61.jpg!200x134"
												width="200" height="134" hover="true"
												alt="测测你是《三生三世十里桃花》里的谁？"></a>
										</p>
										<p class="pinfo">
											<a href="http://www.xinli001.com/ceshi/99897663" class="t">测测你是《三生三世十里桃花》里的谁？</a><br>
											<span class="fgrey">测试人数：</span>61602人<br> <span
												class="fgrey">评论：</span>495条<br> <a
												href="http://www.xinli001.com/ceshi/99897663/start"
												class="start">开始测试</a>
										</p>

									</dt>
									<dd>
										《三生三世十里桃花》，今年的又一IP剧，电影电视一起来。紧跟时代潮流的小编是不会放过这部剧的。嘿，我... <a
											href="http://www.xinli001.com/ceshi/99897663">[详细]</a>
									</dd>
								</dl>
							</div>
							<div class="roll_box">
								<dl>
									<dt>
										<p class="pbox">
											<a href="http://www.xinli001.com/ceshi/99897661"><img
												src="http://ossimg.xinli001.com/20170123/cce8578389fbb6157f98a75970796057.jpg!200x134"
												width="200" height="134" hover="true" alt="情感账户测评"></a>
										</p>
										<p class="pinfo">
											<a href="http://www.xinli001.com/ceshi/99897661" class="t">情感账户测评</a><br>
											<span class="fgrey">测试人数：</span>230人<br> <span
												class="fgrey">评论：</span>0条<br> <a
												href="http://www.xinli001.com/ceshi/99897661/start"
												class="start">开始测试</a>
										</p>

									</dt>
									<dd>
										你是“富翁”还是“负翁”呢？ <a
											href="http://www.xinli001.com/ceshi/99897661">[详细]</a>
									</dd>
								</dl>
							</div>
							<!--part-->
						</div>
					</div>
					<!--滚动集-->
					<div id="roll_sels">
						<ul>
							<li class="act ">
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897673" hover="true">
										<img
										src="http://ossimg.xinli001.com/20170306/146a1564eef13aa62adec2952e196043.jpg!90x60"
										width="60" height="40" alt="测测你像哪个影视剧中的经典角色？我是...">
									</a>
								</p>
								<p class="pinfo">
									<a href="http://www.xinli001.com/ceshi/99897673"
										title="测测你像哪个影视剧中的经典角色？我是...">测测你像哪个影视剧中的经...</a> <br>测试人数：12657人
								</p> <br class="clear"> <span class="arrow"></span>
							</li>
							<li class="">
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/186" hover="true"> <img
										src="http://ossimg.xinli001.com/20170303/eefe9ff5c994502af90b5bbf6127ea50.jpg!90x60"
										width="60" height="40" alt="什么样的异性和你最登对？">
									</a>
								</p>
								<p class="pinfo">
									<a href="http://www.xinli001.com/ceshi/186"
										title="什么样的异性和你最登对？">什么样的异性和你最登对？</a> <br>测试人数：257432人
								</p> <br class="clear"> <span class="arrow"></span>
							</li>
							<li class="">
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897663" hover="true">
										<img
										src="http://ossimg.xinli001.com/20170219/dd9d7bd2913207788d041e8aa736ab61.jpg!90x60"
										width="60" height="40" alt="测测你是《三生三世十里桃花》里的谁？">
									</a>
								</p>
								<p class="pinfo">
									<a href="http://www.xinli001.com/ceshi/99897663"
										title="测测你是《三生三世十里桃花》里的谁？">测测你是《三生三世十里桃...</a> <br>测试人数：61602人
								</p> <br class="clear"> <span class="arrow"></span>
							</li>
							<li class="">
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897661" hover="true">
										<img
										src="http://ossimg.xinli001.com/20170123/cce8578389fbb6157f98a75970796057.jpg!90x60"
										width="60" height="40" alt="情感账户测评">
									</a>
								</p>
								<p class="pinfo">
									<a href="http://www.xinli001.com/ceshi/99897661" title="情感账户测评">情感账户测评</a>
									<br>测试人数：230人
								</p> <br class="clear"> <span class="arrow"></span>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="category">
				<h2 class="title">
					<a href="http://www.xinli001.com/ceshi/fufei">专业测评</a>
				</h2>
				<div class="lists">
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897678"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170314/9d36a67aa175e2b7780856e3e1b27cad.jpg!120x80"
										width="120" height="80" alt="爱与喜欢甄别器" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/99897678"
										title="爱与喜欢甄别器">爱与喜欢甄别器</a><br> 你是喜欢TA还是爱TA？ <span
										class="testers">182人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897507" title="气质类型测评">气质类型测评</a>
								<span class="testers">2894人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897500" title="爱情性格类型测评">爱情性格类型测评</a>
								<span class="testers">5180人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897677" title="失恋成长性评估">失恋成长性评估</a>
								<span class="testers">131人测试</span>
							</dd>
						</dl>
					</div>
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897506"> <img
										class="lazyload"
										src="http://image.xinli001.com/20160302/092007sxnhej079ij86np1.jpg!120x80"
										width="120" height="80" alt="职场竞争力测评" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/99897506"
										title="职场竞争力测评">职场竞争力测评</a><br> 挖掘出你胜人一筹的职场优势 <span
										class="testers">679人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897668" title="恋爱心理成熟度">恋爱心理成熟度</a>
								<span class="testers">294人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897515" title="爱情准备度评估">爱情准备度评估</a>
								<span class="testers">624人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897522" title="情绪智力测评">情绪智力测评</a>
								<span class="testers">2398人测试</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>

			<div class="category">
				<h2 class="title">
					<a href="http://www.xinli001.com/ceshi/amor">爱情测试</a>
				</h2>
				<div class="lists">
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/548"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170313/f6628cfe2e5eedb7567b852bc7334448.jpg!120x80"
										width="120" height="80" alt="你的初恋是什么颜色？" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/548" title="你的初恋是什么颜色？">你的初恋是什么颜色？</a><br>
									每一种颜色能传达出一个人的情绪与心理状态，你的初恋是个什... <span class="testers">41019人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/57"
									title="桃花运测试：哪种颜色增加你的桃花运？">桃花运测试：哪种颜色增加你的桃花运？</a> <span
									class="testers">23371人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/186" title="什么样的异性和你最登对？">什么样的异性和你最登对？</a>
								<span class="testers">257432人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/65" title="你该远离哪种异性？">你该远离哪种异性？</a>
								<span class="testers">45076人测试</span>
							</dd>
						</dl>
					</div>
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/202"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170224/e2e1865c67a449504b88d17d60e6699e.jpg!120x80"
										width="120" height="80" alt="你的“为爱痴狂”指数有多高？" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/202"
										title="你的“为爱痴狂”指数有多高？">你的“为爱痴狂”指数有多高？</a><br>
									爱情对你来说有多重要呢？你会为爱发狂吗？你会变成不是自己... <span class="testers">41580人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/215" title="测测你对爱情的依赖程度">测测你对爱情的依赖程度</a>
								<span class="testers">36675人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/185"
									title="测测来生你和TA的缘分有多深？">测测来生你和TA的缘分有多深？</a> <span
									class="testers">23073人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/184"
									title="桃花运测试：什么时候才会遇上你的正桃花？">桃花运测试：什么时候才会遇上你的正桃...</a> <span
									class="testers">49961人测试</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>

			<div class="category">
				<h2 class="title">
					<a href="http://www.xinli001.com/ceshi/personality">性格测试</a>
				</h2>
				<div class="lists">
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/265"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170314/bf18ebf47f0e122ee65f1f6070489168.jpg!120x80"
										width="120" height="80" alt="你讨厌什么样的自己？" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/265" title="你讨厌什么样的自己？">你讨厌什么样的自己？</a><br>
									你对自己满意吗？你讨厌过自己吗?什么样的自己会让你避而远... <span class="testers">31193人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/882"
									title="测测最适合你的城市类型是什么样子的？">测测最适合你的城市类型是什么样子的？</a> <span
									class="testers">113184人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/214" title="测试你能看见哪种幻想生物">测试你能看见哪种幻想生物</a>
								<span class="testers">22264人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897673"
									title="测测你像哪个影视剧中的经典角色？我是...">测测你像哪个影视剧中的经典角色？我是...</a> <span
									class="testers">12657人测试</span>
							</dd>
						</dl>
					</div>
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/507"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170308/ae20b8e866743f2454e3e33117fb4750.jpg!120x80"
										width="120" height="80" alt="测试你的忧郁感程度" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/507" title="测试你的忧郁感程度">测试你的忧郁感程度</a><br>
									生活压力越来越重，都市人得忧郁症越来越普遍，你忧郁了吗？... <span class="testers">30845人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/261" title="你是属于哪个朝代的人？">你是属于哪个朝代的人？</a>
								<span class="testers">78567人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/248" title="你是什么类型的音乐人？">你是什么类型的音乐人？</a>
								<span class="testers">37574人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897497"
									title="测最适合你的奥斯卡奖项是什么？">测最适合你的奥斯卡奖项是什么？</a> <span
									class="testers">81445人测试</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>

			<div class="category">
				<h2 class="title">
					<a href="http://www.xinli001.com/ceshi/vocational">能力测试</a>
				</h2>
				<div class="lists">
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/177"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170215/7e404600768e73d3caf27ff85ec15a92.jpg!120x80"
										width="120" height="80" alt="测测你的气场有多强？" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/177" title="测测你的气场有多强？">测测你的气场有多强？</a><br>
									气场是一种看不见摸不着但是能感觉到的一种心理状态，气场能... <span class="testers">213366人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/1581" title="测你春节能收到多大红包？">测你春节能收到多大红包？</a>
								<span class="testers">188750人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/147" title="你的梦想离现实有多远？">你的梦想离现实有多远？</a>
								<span class="testers">67973人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897654"
									title="记忆测试丨哪张图片才是对的？">记忆测试丨哪张图片才是对的？</a> <span class="testers">48346人测试</span>
							</dd>
						</dl>
					</div>
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897653"> <img
										class="lazyload"
										src="http://ossimg.xinli001.com/20170109/a35a26102741701042a855a1ad00fd14.jpg!120x80"
										width="120" height="80" alt="测你能否鸡年行大运？" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/99897653"
										title="测你能否鸡年行大运？">测你能否鸡年行大运？</a><br>
									新的一年，谁都希望自己大吉大利，你又会展开怎样的新篇章呢... <span class="testers">32134人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897651"
									title="色盲测试丨挑战你的辨色能力">色盲测试丨挑战你的辨色能力</a> <span class="testers">42951人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/86347370"
									title="测测最适合你的新年计划？">测测最适合你的新年计划？</a> <span class="testers">95693人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897644"
									title="测测你是哪位神仙转世">测测你是哪位神仙转世</a> <span class="testers">60231人测试</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>

			<div class="category">
				<h2 class="title">
					<a href="http://www.xinli001.com/ceshi/member">会员测试</a>
				</h2>
				<div class="lists">
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/362"> <img
										class="lazyload"
										src="http://image.xinli001.com/20160704/075300tqnqi6688wv3f3dd.jpg!120x80"
										width="120" height="80" alt="深度揭秘：看看你有多黑暗！" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/362"
										title="深度揭秘：看看你有多黑暗！">深度揭秘：看看你有多黑暗！</a><br>
									测试前提：这是一个梦。灰色而压抑的梦境。当然，你身处梦中... <span class="testers">224220人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/2120" title="测你是哪位超级英雄">测你是哪位超级英雄</a>
								<span class="testers">149282人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897551"
									title="你的内心到底有多孤独？">你的内心到底有多孤独？</a> <span class="testers">110936人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897547"
									title="谁是最适合你的另一半">谁是最适合你的另一半</a> <span class="testers">123017人测试</span>
							</dd>
						</dl>
					</div>
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/99897533"> <img
										class="lazyload"
										src="http://image.xinli001.com/20160322/112454y8201zcualdga9zt.jpg!120x80"
										width="120" height="80" alt="色彩错觉测试丨不要相信你的眼睛" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/99897533"
										title="色彩错觉测试丨不要相信你的眼睛">色彩错觉测试丨不要相信你的眼睛</a><br>
									当视觉受到周围环境色彩的影响时，就会产生对色彩的错觉现象... <span class="testers">146545人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897532"
									title="测测你的恋爱智商有多高">测测你的恋爱智商有多高</a> <span class="testers">170472人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897524"
									title="测你在异性眼中的第一印象">测你在异性眼中的第一印象</a> <span class="testers">165222人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897523"
									title="测测你不为人知的的缺点是什么？">测测你不为人知的的缺点是什么？</a> <span
									class="testers">121310人测试</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>

			<div class="category nobor">
				<h2 class="title">
					<a href="http://www.xinli001.com/ceshi/professional">精选测试</a>
				</h2>
				<div class="lists">
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/495"> <img
										class="lazyload"
										src="http://image.xinli001.com/20160712/02232890pm9d9ml580x9b9.jpg!120x80"
										width="120" height="80" alt="孤独感测试" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/495" title="孤独感测试">孤独感测试</a><br>
									孤独量表评价由于对社会交往的渴望与实际水平的差距而产生的... <span class="testers">172970人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/499" title="标准情商（EQ）测试">标准情商（EQ）测试</a>
								<span class="testers">989040人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/2057" title="爱情依恋模式测评">爱情依恋模式测评</a>
								<span class="testers">142775人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/395" title="心理年龄测试">心理年龄测试</a>
								<span class="testers">420719人测试</span>
							</dd>
						</dl>
					</div>
					<div class="parts">
						<dl>
							<dt>
								<p class="pbox">
									<a href="http://www.xinli001.com/ceshi/39"> <img
										class="lazyload"
										src="http://image.xinli001.com/ceshi/20111031/20111031090940-44.jpg!120x80"
										width="120" height="80" alt="菲尔人格测试" hover="true" />
									</a>
								</p>
								<p class="cinfo">
									<a href="http://www.xinli001.com/ceshi/39" title="菲尔人格测试">菲尔人格测试</a><br>
									这是一个目前很多大公司人事部门实际采用的测试。 <span class="testers">240765人测试</span>
								</p>
							</dt>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/99897477"
									title="如何交朋友？先测下自己的交友能力吧">如何交朋友？先测下自己的交友能力吧</a> <span
									class="testers">160885人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/470"
									title="拖延症测试：你拖延到什么程度了？">拖延症测试：你拖延到什么程度了？</a> <span
									class="testers">125145人测试</span>
							</dd>
							<dd>
								<span class="fcn">·</span> <a
									href="http://www.xinli001.com/ceshi/513" title="自信心测试：你有多自信？">自信心测试：你有多自信？</a>
								<span class="testers">70268人测试</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!--left-->
		<div class="fish-left main-right">
			<div class="multi_ads fixed">
				<div class="mt">
					<strong>编辑推荐<span class="mq">：</span><span class="ms">······
					</span></strong>
				</div>
				<dl>
					<dd>
						<a href="http://www.xinli001.com/info/100360756?from=cheshi"
							title="心探社" target="_blank"> <img
							src="http://ossimg.xinli001.com/20161219/22c35b0aef597efe978b7a084dcf3030.jpg"
							width="135" height="135" hover="true" title="心探社" alt="心探社">
						</a>
					</dd>

					<dd>
						<a href="http://www.xinli001.com/ceshi/99897526?from=2017"
							title="心理需求测评" target="_blank"> <img
							src="http://image.xinli001.com/20160805/030954q4tnzb0jb0nyq06s.jpg"
							width="135" height="135" hover="true" title="心理需求测评" alt="心理需求测评">
						</a>
					</dd>

					<dt>
						<a href="http://www.xinli001.com/ceshi/99897572?from=ceshi"
							title="抑郁症测试" target="_blank"> <img
							src="http://ossimg.xinli001.com/20161024/16f19e49dab19387649e80eae35df090.jpg"
							width="280" height="135" hover="true" title="抑郁症测试" alt="抑郁症测试">
						</a>
					</dt>
					<br class="clear">
				</dl>
			</div>
			<div class="test_rmd_list fixed">
				<div class="st rbor">
					<span class="fb">热门测试 ：</span> <span class="more"> <a
						href="javascript:void(0)" data-page="2"
						data-url="http://www.xinli001.com/ceshi/hot-ceshis"
						onclick="changeHotCeshi(this)">换一换</a>
					</span>
				</div>
				<dl id="id_hot_ceshi_list">
					<dd>
						<p class="pbox fl">
							<a href="http://www.xinli001.com/ceshi/422"> <img
								src="http://image.xinli001.com/20160606/022811jqm9odk4b73huws9.jpg!90x60"
								width="90" height="60" hover="true" alt="精选测试：MBTI职业性格测试" />
							</a>
						</p>
						<p class="tinfo fl">
							<a href="http://www.xinli001.com/ceshi/422">精选测试：MBTI职业性格测试</a> <span
								class="total"><span class="icons" title="测试人数"></span>1736811人测试过</span>
						</p>
					</dd>
					<dd class="rlists">
						<p class="items">
							<span class="fcn">·</span><a
								href="http://www.xinli001.com/ceshi/1822">测你性格最真实的一面</a> <span
								class="total"><span class="icons" title="测试人数"></span>1628330人</span>
						</p>
						<p class="items">
							<span class="fcn">·</span><a
								href="http://www.xinli001.com/ceshi/99897392">测你恋爱时有多投入？</a> <span
								class="total"><span class="icons" title="测试人数"></span>1485190人</span>
						</p>
						<p class="items">
							<span class="fcn">·</span><a
								href="http://www.xinli001.com/ceshi/385">测测你的同性恋潜质</a> <span
								class="total"><span class="icons" title="测试人数"></span>1483947人</span>
						</p>
						<p class="items">
							<span class="fcn">·</span><a
								href="http://www.xinli001.com/ceshi/99897391">测你最容易有哪种负面情绪？</a>
							<span class="total"><span class="icons" title="测试人数"></span>1380136人</span>
						</p>
						<p class="items">
							<span class="fcn">·</span><a
								href="http://www.xinli001.com/ceshi/99897393">弗洛伊德性格测试</a> <span
								class="total"><span class="icons" title="测试人数"></span>1302873人</span>
						</p>
						<p class="items">
							<span class="fcn">·</span><a
								href="http://www.xinli001.com/ceshi/99897397">超准职业测试：测你最适合干什么样的工作？</a>
							<span class="total"><span class="icons" title="测试人数"></span>1299086人</span>
						</p>
					</dd>
				</dl>
			</div>
			<div class="tags fixed">
				<div class="st rbor">
					<span class="fb">热门标签 ：</span>
				</div>
				<ul>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%80%A7%E6%A0%BC%E6%B5%8B%E8%AF%95">性格测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E7%88%B1%E6%83%85%E6%B5%8B%E8%AF%95">爱情测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%83%BD%E5%8A%9B%E6%B5%8B%E8%AF%95">能力测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%BF%83%E7%90%86%E6%B5%8B%E8%AF%95">心理测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%B6%A3%E5%91%B3%E6%B5%8B%E8%AF%95">趣味测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E4%B8%93%E4%B8%9A%E6%B5%8B%E8%AF%95">专业测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%A7%BB%E7%BC%98%E6%B5%8B%E8%AF%95">姻缘测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%B5%8B%E8%AF%95%E7%88%B1%E6%83%85">测试爱情</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%81%8C%E4%B8%9A%E6%B5%8B%E8%AF%95">职业测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E9%A2%84%E8%A8%80%E6%B5%8B%E8%AF%95">预言测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E4%BC%9A%E5%91%98%E6%B5%8B%E8%AF%95">会员测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E7%88%B1%E6%83%85">爱情</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E7%BF%BB%E8%AF%91%E6%B5%8B%E8%AF%95">翻译测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E4%B8%93%E4%B8%9A%E9%87%8F%E8%A1%A8">专业量表</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%BD%9C%E6%84%8F%E8%AF%86%E6%B5%8B%E8%AF%95">潜意识测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%A4%96%E5%9B%BD%E6%B5%8B%E8%AF%95">外国测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%80%A7%E6%A0%BC">性格</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E8%81%8C%E5%9C%BA%E6%B5%8B%E8%AF%95">职场测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%81%A5%E5%BA%B7%E6%B5%8B%E8%AF%95">健康测试</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E5%BF%83%E7%90%86%E5%81%A5%E5%BA%B7">心理健康</a></li>
					<li class="fl"><a
						href="http://www.xinli001.com/ceshi/tag?name=%E6%81%8B%E7%88%B1">恋爱</a></li>
				</ul>
			</div>
			<!--标签-->
		</div>
		<!--right-->
	</div>

	<%@ include file="/common/include/footer.jsp" %>
	
</body>
</html>