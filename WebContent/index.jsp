<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>导航</title>
	</head>
	<body>
		<table width="90%" border="1">
			<tr>
				<td>名称</td>
				<td>链接</td>
			</tr>
			<tr>
				<td>管理员列表</td>
				<td><a href="${rootPath}/adminsList.mvc" target="_blank">管理员列表</a></td>
			</tr>
			<tr>
				<td>壹心理首页</td>
				<td><a href="${rootPath}/main.mvc" target="_blank">首页</a></td>
			</tr>
		</table>
	</body>
</html>