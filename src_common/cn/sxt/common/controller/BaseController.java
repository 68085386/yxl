package cn.sxt.common.controller;

import javax.servlet.http.HttpServletRequest;

import cn.sxt.common.util.PageInfoUtil;

public class BaseController {

	//生成分页对象
	protected PageInfoUtil proccedPageInfo(HttpServletRequest request)
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		String page = request.getParameter("page");
		try
		{
			pageInfoUtil.setCurrentPage(Integer.valueOf(page));
		} catch (NumberFormatException e)
		{
		}
		return pageInfoUtil;
	}
}
