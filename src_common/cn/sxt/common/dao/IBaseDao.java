package cn.sxt.common.dao;

import java.util.List;
import java.util.Map;

import cn.sxt.common.util.PageInfoUtil;

public interface IBaseDao<T> {
	T findOne(Map<String, Object> condMap);
	
	int saveOne(T t);
	
	int updateOne(T t);
	
	int deleteOne(T t);
	
	List<T> findAll(Map<String, Object> condMap);
}
