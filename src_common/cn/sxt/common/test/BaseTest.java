package cn.sxt.common.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.sxt.common.util.ConstatFinalUtil;

public class BaseTest {
	
	protected ApplicationContext ac;
	
	@Before
	public void init(){
		//加载spring文件
		this.ac = new ClassPathXmlApplicationContext("spring/applicationContext_*.xml");
		ConstatFinalUtil.LOGGER.info("---init---");
	}
	
	@Test
	public void test(){
		ConstatFinalUtil.LOGGER.info("---test---");
	}
	
	@After
	public void close(){
		if(this.ac instanceof ClassPathXmlApplicationContext){
			ClassPathXmlApplicationContext cac = (ClassPathXmlApplicationContext) this.ac;
			cac.close();
		}
		ConstatFinalUtil.LOGGER.info("---close---");
	}
}
