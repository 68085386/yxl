package cn.sxt.common.util;

/**
 * 分页工具类
 * 
 * @author wangshSxt
 *
 */
public class PageInfoUtil
{
	/* 总记录数 */
	private int totalRecord;
	/* 每页多少条 */
	private int pageSize = 10;
	/* 当前页 */
	private int currentPage;
	/* 上一页 */
	private int prePage;
	/* 下一页 */
	private int nextPage;
	/* 总页数 */
	private int totalPage;
	/* 当前页的条数 */
	private int currentRecord;

	public int getTotalRecord()
	{
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord)
	{
		this.totalRecord = totalRecord;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public int getCurrentPage()
	{
		/* 对当前页进行一些判断 */
		if(this.currentPage < 1)
		{
			this.currentPage = 1 ; 
		}
		
		if(this.currentPage > this.getTotalPage() && this.getTotalPage() > 0 )
		{
			this.currentPage = this.getTotalPage()  ;
		}
		return currentPage;
	}

	public void setCurrentPage(int currentPage)
	{
		this.currentPage = currentPage;
	}

	public int getPrePage()
	{
		this.prePage = this.getCurrentPage() - 1 ;
		if(this.prePage < 1 )
		{
			this.prePage = 1 ;
		}
		return prePage;
	}

	public int getNextPage()
	{
		this.nextPage = this.getCurrentPage() + 1 ;
		if(this.nextPage > this.getTotalPage() && this.getTotalPage() > 0)
		{
			this.nextPage = this.getTotalPage() ;
		}
		return nextPage;
	}

	public int getTotalPage()
	{
		/* 总页数 */
		this.totalPage = (int)Math.ceil(1.0 * this.getTotalRecord() / this.pageSize); 
		return totalPage;
	}

	public int getCurrentRecord()
	{
		/* 当前记录数
		 * 每页10条;
		 * 
		 * 1	10 	1
		 * 11	10 	2
		 * 21	10 	3
		 *  */
		this.currentRecord = (this.getCurrentPage() - 1) * this.pageSize;
		return currentRecord;
	}
	
	
	@Override
	public String toString()
	{
		return "PageInfoUtil [totalRecord=" + totalRecord + ", pageSize=" + pageSize + ", currentPage=" + this.getCurrentPage()
				+ ", prePage=" + this.getPrePage() + ", nextPage=" + this.getNextPage() + ", totalPage=" + this.getTotalPage() + ", currentRecord="
				+ this.getCurrentRecord() + "]";
	}

	public static void main(String[] args)
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setTotalRecord(41);
		pageInfoUtil.setPageSize(10);
		pageInfoUtil.setCurrentPage(13);
		
		System.out.println(pageInfoUtil);
	}

}
