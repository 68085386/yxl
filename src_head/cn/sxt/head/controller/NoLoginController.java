package cn.sxt.head.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.sxt.common.util.ConstatFinalUtil;
import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.user.pojo.AAdmins;
import cn.sxt.user.service.IUserService;



@Controller
public class NoLoginController
{
	@Resource
	private IUserService userService;
	
	/**
	 * 管理员列表
	 * @return
	 */
	@RequestMapping("/adminsList")
	public String adminsList(HttpServletRequest request)
	{
		ConstatFinalUtil.LOGGER.info("---- adminsList -----");
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		/*List<AAdmins> adminsList = this.usersService.findCondListAdminsService(null, condMap);*/
		List<AAdmins> adminsList = this.userService.findAllAdmins(pageInfoUtil, condMap);
		request.setAttribute("adminsList", adminsList);
		return "/head/adminsList" ; 
	}
}
