package cn.sxt.head.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.sxt.common.controller.BaseController;
import cn.sxt.common.util.ConstatFinalUtil;
import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.question.pojo.AColumns;
import cn.sxt.question.pojo.AColumnsEnum;
import cn.sxt.question.pojo.AItems;
import cn.sxt.question.pojo.AItemsEnum;
import cn.sxt.question.service.IQuestionService;

@Controller
public class YxlHeadController extends BaseController{

	@Resource
	private IQuestionService questionService;
	
	//主页面
	@RequestMapping("/main")
	public String main(){
		ConstatFinalUtil.LOGGER.info("--main--");
		
		return "/head/main";
	}
	//查询所有的栏目
	@RequestMapping("/header")
	public String header(HttpServletRequest request){
		ConstatFinalUtil.LOGGER.info("--header--");
		//查询所有的栏目
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("status", AColumnsEnum.STATUS_ENABLE.getStatus());
		List<AColumns> columnsList = questionService.findAllColumns(null, condMap);
		request.setAttribute("columnsList", columnsList);
		return "/head/header";
	}
	
	//items页面
	@RequestMapping("/{url}")
	public String itemsList(HttpServletRequest request,@PathVariable String url){
		ConstatFinalUtil.LOGGER.info("--itemsList--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 查询url对应的栏目 */
		condMap.clear();
		condMap.put("url", url);
		AColumns columns = this.questionService.findOneColumns(condMap);
		request.setAttribute("columns", columns);
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 查询url对应的栏目下面的测试项信息
		 * 启用 */
		condMap.clear();
		condMap.put("columnsId", columns.getId());
		condMap.put("status", AItemsEnum.STATUS_ENABLE.getStatus());
		List<AItems> itemsList = this.questionService.findAllItems(pageInfoUtil, condMap);
		/* 测试项列表 */
		request.setAttribute("pageInfoUtil", pageInfoUtil);
		request.setAttribute("itemsList", itemsList);
		return "/head/itemsList";
	}
	
	//items详情页面
	@RequestMapping("/items/{iid}")
	public String itemsInfo(HttpServletRequest request,@PathVariable int iid){
		ConstatFinalUtil.LOGGER.info("--itemsInfo--");
		/* 根据Id查询测试项 */
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.clear();
		condMap.put("id", iid);
		AItems items = this.questionService.findOneItems(condMap);
		/* 将查询出的结果放到request */
		request.setAttribute("items", items);
		
		/* 查询上一条 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		/*pageInfoUtil.setPageSize(1);*/
		condMap.clear();
		condMap.put("pubTimeDa", items.getPubTime());
		condMap.put("orderby", "pubTimeASC");
		List<AItems> itemsPreList = this.questionService.findAllItems(pageInfoUtil, condMap);
		request.setAttribute("itemsPreList", itemsPreList);
		
		/* 查询下一条 */
		condMap.clear();
		condMap.put("pubTimeXiao", items.getPubTime());
		List<AItems> itemsNextList = this.questionService.findAllItems(pageInfoUtil, condMap);
		request.setAttribute("itemsNextList", itemsNextList);
		return "/head/itemsInfo" ; 
	}
	
	//问题列表
	@RequestMapping("/items/{iid}/start")
	public String questionList(HttpServletRequest request,@PathVariable int iid){
		ConstatFinalUtil.LOGGER.info("--questionList--");
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.clear();
		condMap.put("id", iid);
		AItems items = this.questionService.findOneItems(condMap);
		/* 将查询出的结果放到request */
		request.setAttribute("items", items);
		return "/head/questionList";
	}
}
