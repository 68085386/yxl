package cn.sxt.question.dao;

import cn.sxt.common.dao.IBaseDao;
import cn.sxt.question.pojo.AAnswer;


public interface IAAnswerDao extends IBaseDao<AAnswer>
{
	
}
