package cn.sxt.question.dao;

import cn.sxt.common.dao.IBaseDao;
import cn.sxt.question.pojo.AColumns;
import cn.sxt.question.pojo.AItems;

public interface IAItemsDao extends IBaseDao<AItems>{

}
