package cn.sxt.question.dao;

import cn.sxt.common.dao.IBaseDao;
import cn.sxt.question.pojo.AQuestion;


public interface IAQuestionDao extends IBaseDao<AQuestion>
{
	
}
