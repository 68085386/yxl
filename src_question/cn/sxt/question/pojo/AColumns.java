package cn.sxt.question.pojo;

import java.util.Date;

public class AColumns {

	
	private int id;
	private String name;
	private String content;
	private String url;
	private byte flag;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	private String statusStr;
	private String flagStr;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Date getPubTime() {
		return pubTime;
	}
	public void setPubTime(Date pubTime) {
		this.pubTime = pubTime;
	}
	public String getStatusStr() {
		/* 从枚举中取值 */
		AColumnsEnum[] columnsEnums = AColumnsEnum.values() ;
		for (int i = 0; i < columnsEnums.length; i++)
		{
			AColumnsEnum columnsEnum = columnsEnums[i];
			if(columnsEnum.toString().startsWith("STATUS_")
				&& columnsEnum.getStatus() == this.getStatus())
			{
				this.statusStr = columnsEnum.getName() ; 
			}
		}
		return statusStr;
	}
	public String getFlagStr() {
		/* 从枚举中取值 */
		AColumnsEnum[] columnsEnums = AColumnsEnum.values() ;
		for (int i = 0; i < columnsEnums.length; i++)
		{
			AColumnsEnum columnsEnum = columnsEnums[i];
			if(columnsEnum.toString().startsWith("FLAG_")
				&& columnsEnum.getStatus() == this.getStatus())
			{
				this.flagStr = columnsEnum.getName() ; 
			}
		}
		return flagStr;
	}
	
	
}
