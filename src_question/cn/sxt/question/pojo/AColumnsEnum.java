package cn.sxt.question.pojo;

public enum AColumnsEnum {

	//状态
	STATUS_ENABLE(Byte.valueOf("1"),"启用"),
	STATUS_DISABLE(Byte.valueOf("0"),"禁用"),
	
	//标识
	FLAG_NO(Byte.valueOf("0"),"无"),
	FLAG_NEW(Byte.valueOf("10"),"新"),
	FLAG_HOT(Byte.valueOf("20"),"热门");
	
	private byte status;
	private String name;
	
	private AColumnsEnum(byte status, String name) {
		this.status = status;
		this.name = name;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
