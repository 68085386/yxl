package cn.sxt.question.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.question.pojo.AAnswer;
import cn.sxt.question.pojo.AColumns;
import cn.sxt.question.pojo.AItems;
import cn.sxt.question.pojo.AQuestion;

public interface IQuestionService {
	//栏目模块开始
	AColumns findOneColumns(Map<String, Object> condMap);
	
	JSONObject saveOneColumns(AColumns columns);
	
	JSONObject updateOneColumns(AColumns columns);
	
	JSONObject deleteOneColumns(AColumns columns);
	
	List<AColumns> findAllColumns(PageInfoUtil pageInfoUtil,Map<String, Object> condMap);
	//栏目模块结束
	
	
	//测试模块开始
	AItems findOneItems(Map<String, Object> condMap);
	
	JSONObject saveOneItems(AItems items);
	
	JSONObject updateOneItems(AItems items);
	
	JSONObject deleteOneItems(AItems items);
	
	List<AItems> findAllItems(PageInfoUtil pageInfoUtil,Map<String, Object> condMap);
	//测试模块结束
	
	//答案模块开始
	AAnswer findOneAnswer(Map<String, Object> condMap);
	
	JSONObject saveOneAnswer(AAnswer answer);
	
	JSONObject updateOneAnswer(AAnswer answer);
	
	JSONObject deleteOneAnswer(AAnswer answer);
	
	List<AAnswer> findAllAnswer(PageInfoUtil pageInfoUtil,Map<String, Object> condMap);
	//答案模块结束
	
	//问题模块开始
	AQuestion findOneQuestion(Map<String, Object> condMap);
	
	JSONObject saveOneQuestion(AQuestion question);
	
	JSONObject updateOneQuestion(AQuestion question);
	
	JSONObject deleteOneQuestion(AQuestion question);
	
	List<AQuestion> findAllQuestion(PageInfoUtil pageInfoUtil,Map<String, Object> condMap);
	//问题模块结束
}
