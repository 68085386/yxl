package cn.sxt.question.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.question.dao.IAAnswerDao;
import cn.sxt.question.dao.IAColumnsDao;
import cn.sxt.question.dao.IAItemsDao;
import cn.sxt.question.dao.IAQuestionDao;
import cn.sxt.question.pojo.AAnswer;
import cn.sxt.question.pojo.AColumns;
import cn.sxt.question.pojo.AItems;
import cn.sxt.question.pojo.AQuestion;
import cn.sxt.question.service.IQuestionService;

@Service("questionService")
public class QuestionServiceImpl implements IQuestionService{
	@Resource
	private IAColumnsDao columnsDao;
	@Resource
	private IAItemsDao itemsDao;
	@Resource
	private IAAnswerDao answerDao;
	@Resource
	private IAQuestionDao questionDao;
	//栏目模块开始
	@Override
	public AColumns findOneColumns(Map<String, Object> condMap) {
		return columnsDao.findOne(condMap);
	}

	@Override
	public JSONObject saveOneColumns(AColumns columns) {
		JSONObject jsonObject = new JSONObject();
		int res = columnsDao.saveOne(columns);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}

	@Override
	public JSONObject updateOneColumns(AColumns columns) {
		JSONObject jsonObject = new JSONObject();
		int res = this.columnsDao.updateOne(columns);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}

	@Override
	public JSONObject deleteOneColumns(AColumns columns) {
		JSONObject jsonObject = new JSONObject();
		int res = this.columnsDao.deleteOne(columns);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		return jsonObject;
	}

	@Override
	public List<AColumns> findAllColumns(PageInfoUtil pageInfoUtil,Map<String, Object> condMap) {
		if(pageInfoUtil!=null){
			
			//分页
			Page<AColumns> page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			List<AColumns> columnslist= this.columnsDao.findAll(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			return columnslist;
		}
		
		return this.columnsDao.findAll(condMap);
	}
	//栏目模块结束
	//测试模块开始
	@Override
	public AItems findOneItems(Map<String, Object> condMap) {
		return itemsDao.findOne(condMap);
	}
	
	@Override
	public JSONObject saveOneItems(AItems items) {
		JSONObject jsonObject = new JSONObject();
		int res = itemsDao.saveOne(items);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}
	
	@Override
	public JSONObject updateOneItems(AItems items) {
		JSONObject jsonObject = new JSONObject();
		int res = this.itemsDao.updateOne(items);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}
	
	@Override
	public JSONObject deleteOneItems(AItems items) {
		JSONObject jsonObject = new JSONObject();
		int res = this.itemsDao.deleteOne(items);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		return jsonObject;
	}
	
	@Override
	public List<AItems> findAllItems(PageInfoUtil pageInfoUtil,Map<String, Object> condMap) {
		if(pageInfoUtil!=null){
			
			//分页
			Page<AItems> page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			List<AItems> itemslist= this.itemsDao.findAll(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			return itemslist;
		}
		
		return this.itemsDao.findAll(condMap);
	}
	//测试模块结束
	
	//答案模块开始
	@Override
	public AAnswer findOneAnswer(Map<String, Object> condMap) {
		return answerDao.findOne(condMap);
	}
	
	@Override
	public JSONObject saveOneAnswer(AAnswer answer) {
		JSONObject jsonObject = new JSONObject();
		int res = answerDao.saveOne(answer);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}
	
	@Override
	public JSONObject updateOneAnswer(AAnswer answer) {
		JSONObject jsonObject = new JSONObject();
		int res = this.answerDao.updateOne(answer);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}
	
	@Override
	public JSONObject deleteOneAnswer(AAnswer answer) {
		JSONObject jsonObject = new JSONObject();
		int res = this.answerDao.deleteOne(answer);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		return jsonObject;
	}
	
	@Override
	public List<AAnswer> findAllAnswer(PageInfoUtil pageInfoUtil,Map<String, Object> condMap) {
		if(pageInfoUtil!=null){
			
			//分页
			Page<AAnswer> page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			List<AAnswer> answerlist= this.answerDao.findAll(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			return answerlist;
		}
		
		return this.answerDao.findAll(condMap);
	}
	//答案模块结束
	
	//问题模块开始
		@Override
		public AQuestion findOneQuestion(Map<String, Object> condMap) {
			return questionDao.findOne(condMap);
		}
		
		@Override
		public JSONObject saveOneQuestion(AQuestion question) {
			JSONObject jsonObject = new JSONObject();
			int res = questionDao.saveOne(question);
			if(res>0){
				jsonObject.put("code", "1");
				jsonObject.put("info", "成功");
			}else{
				jsonObject.put("code", "0");
				jsonObject.put("info", "失败");			
			}
			
			return jsonObject;
		}
		
		@Override
		public JSONObject updateOneQuestion(AQuestion question) {
			JSONObject jsonObject = new JSONObject();
			int res = this.questionDao.updateOne(question);
			if(res>0){
				jsonObject.put("code", "1");
				jsonObject.put("info", "成功");
			}else{
				jsonObject.put("code", "0");
				jsonObject.put("info", "失败");			
			}
			
			return jsonObject;
		}
		
		@Override
		public JSONObject deleteOneQuestion(AQuestion question) {
			JSONObject jsonObject = new JSONObject();
			int res = this.questionDao.deleteOne(question);
			if(res>0){
				jsonObject.put("code", "1");
				jsonObject.put("info", "成功");
			}else{
				jsonObject.put("code", "0");
				jsonObject.put("info", "失败");			
			}
			return jsonObject;
		}
		
		@Override
		public List<AQuestion> findAllQuestion(PageInfoUtil pageInfoUtil,Map<String, Object> condMap) {
			if(pageInfoUtil!=null){
				
				//分页
				Page<AQuestion> page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
				List<AQuestion> questionlist= this.questionDao.findAll(condMap);
				pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
				return questionlist;
			}
			
			return this.questionDao.findAll(condMap);
		}
		//问题模块结束
}
