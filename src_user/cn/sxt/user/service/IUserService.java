package cn.sxt.user.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.user.pojo.AAdmins;

public interface IUserService {

	//管理员模块开始
	AAdmins findOneAdmins(Map<String, Object> condMap);
	
	JSONObject saveOneAdmins(AAdmins admins);
	
	JSONObject updateOneAdmins(AAdmins admins);
	
	JSONObject deleteOneAdmins(AAdmins admins);
	
	List<AAdmins> findAllAdmins(PageInfoUtil pageInfoUtil,Map<String, Object> condMap);
	//管理员模块结束
}
