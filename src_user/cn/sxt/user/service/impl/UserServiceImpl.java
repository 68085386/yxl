package cn.sxt.user.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.user.dao.IAAdminsDao;
import cn.sxt.user.pojo.AAdmins;
import cn.sxt.user.service.IUserService;

@Service("userService")
public class UserServiceImpl implements IUserService{

	@Resource
	private IAAdminsDao adminsDao;
	
	@Override
	public AAdmins findOneAdmins(Map<String, Object> condMap) {
		return adminsDao.findOne(condMap);
	}

	@Override
	public JSONObject saveOneAdmins(AAdmins admins) {
		JSONObject jsonObject = new JSONObject();
		int res = adminsDao.saveOne(admins);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}

	@Override
	public JSONObject updateOneAdmins(AAdmins admins) {
		JSONObject jsonObject = new JSONObject();
		int res = this.adminsDao.updateOne(admins);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		
		return jsonObject;
	}

	@Override
	public JSONObject deleteOneAdmins(AAdmins admins) {
		JSONObject jsonObject = new JSONObject();
		int res = this.adminsDao.deleteOne(admins);
		if(res>0){
			jsonObject.put("code", "1");
			jsonObject.put("info", "成功");
		}else{
			jsonObject.put("code", "0");
			jsonObject.put("info", "失败");			
		}
		return jsonObject;
	}

	@Override
	public List<AAdmins> findAllAdmins(PageInfoUtil pageInfoUtil,Map<String, Object> condMap) {
		if(pageInfoUtil!=null){
			
			//分页
			Page<AAdmins> page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			List<AAdmins> adminslist= this.adminsDao.findAll(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			return adminslist;
		}
		
		return this.adminsDao.findAll(condMap);
	}

}
