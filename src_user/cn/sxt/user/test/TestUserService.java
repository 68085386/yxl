package cn.sxt.user.test;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

import cn.sxt.common.test.BaseTest;
import cn.sxt.common.util.ConstatFinalUtil;
import cn.sxt.common.util.PageInfoUtil;
import cn.sxt.user.pojo.AAdmins;
import cn.sxt.user.service.IUserService;

public class TestUserService extends BaseTest{

	private IUserService userService;
	
	@Before
	public void init(){
		super.init();
		this.userService = (IUserService) this.ac.getBean("userService");
		ConstatFinalUtil.LOGGER.info("--test init--");
	}
	
	@Test
	public void findOneAdmins(){
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", 17);
		AAdmins admins = this.userService.findOneAdmins(condMap);
		ConstatFinalUtil.LOGGER.info("id:{},email:{},trueName:{}",admins.getId(),admins.getEmail(),admins.getTrueName());
	}
	
	@Test
	public void saveOneAdmins(){
		AAdmins admins = new AAdmins();
		admins.setEmail("111");
		admins.setPassword("111");
		admins.setPhone("111");
		admins.setQq("111");
		admins.setTrueName("111");
		admins.setCreateTime(new Date());
		admins.setUpdateTime(new Date());
		admins.setLastLoginTime(new Date());
		JSONObject jsonObject = this.userService.saveOneAdmins(admins);
		ConstatFinalUtil.LOGGER.info("结果:{}",jsonObject);
	}
	
	@Test
	public void updateOneAdmins(){
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", 7);
		AAdmins admins = this.userService.findOneAdmins(condMap);
		admins.setEmail("666");
		JSONObject jsonObject = this.userService.updateOneAdmins(admins);
		ConstatFinalUtil.LOGGER.info("结果:{}",jsonObject);
	}
	
	@Test
	public void deleteOneAdmins(){
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", 7);
		AAdmins admins = this.userService.findOneAdmins(condMap);
		JSONObject jsonObject = this.userService.deleteOneAdmins(admins);
		ConstatFinalUtil.LOGGER.info("结果:{}",jsonObject);
	}
	
	@Test
	public void findAllAdmins(){
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("status", 1);
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setCurrentPage(1);
		pageInfoUtil.setPageSize(2);
		List<AAdmins> adminsList = this.userService.findAllAdmins(pageInfoUtil, condMap);
		int count = 1;
		for (AAdmins admins : adminsList) {
			ConstatFinalUtil.LOGGER.info("计数:{},id:{},email:{},creatTime:{}",
					count ,admins.getId(),admins.getEmail(),admins.getCreateTime().toLocaleString());
			count ++ ; 
		}
		ConstatFinalUtil.LOGGER.info("总记录数:{},总页数:{},当前页:{},每页条数:{}",
				pageInfoUtil.getTotalRecord(),pageInfoUtil.getTotalPage(),pageInfoUtil.getCurrentPage(),
				pageInfoUtil.getPageSize());
	}
}
